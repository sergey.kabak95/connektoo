var gulp          = require('gulp'),
    gutil         = require('gulp-util' ),               //вспомогательные ф-ции для gulp(логирование, подсветка вывода в консоли и т.д.)
    sass          = require('gulp-sass'),                //работа с Sass
    browsersync   = require('browser-sync'),             //LivePreview
    concat        = require('gulp-concat'),              //конкатенация файлов
    uglify        = require('gulp-uglify'),              //минимизация файлов
    cleanCSS      = require('gulp-clean-css'),           //минификация CSS
    rename        = require('gulp-rename'),              //переименовывает файлы(нужно для *.min.*)
    del           = require('del'),                      //удаляет файлы
    autoprefixer  = require('gulp-autoprefixer'),        //для автоматических вендорных префиксов
    notify        = require("gulp-notify"),              //плагин оповещений для gulp
    rsync         = require('gulp-rsync');               //загрузка файлов на сервер

gulp.task('browser-sync', function() {
  browsersync({
    server: {
      baseDir: 'app'
    },
    notify: false,
    // open: false,
    // tunnel: true,
    // tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
  })
});

gulp.task('sass', function() {
  return gulp.src('app/sass/**/*.sass')
  .pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
  // .pipe(rename({ suffix: '.min', prefix : '' }))
  .pipe(autoprefixer(['last 15 versions']))
  .pipe(cleanCSS( {level: { 1: { specialComments: 'all' } } })) // Opt., comment out when debugging
  .pipe(concat('main.min.css'))
  .pipe(gulp.dest('app/css'))
  .pipe(browsersync.reload( {stream: true} ))
});

// gulp.task('libs-min', function () {
//   return gulp.src('app/css/libs.css')
//   // .pipe(rename({ suffix: '.min', prefix : '' }))
//   .pipe(cleanCSS())
//   .pipe(gulp.dest('dist/css'));
// });

gulp.task('common-js', function() {
  return gulp.src([
    'app/js/common.js',
    ])
  //.pipe(concat('common.min.js'))
  //.pipe(uglify())
  .pipe(gulp.dest('app/js'));
});

gulp.task('js', ['common-js'], function() {
  return gulp.src([
    'app/libs/jquery/dist/jquery.min.js',
    'app/libs/bootstrap/js/bootstrap.bundle.min.js',
    'app/js/common.js', // Always at the end
    ])
  .pipe(concat('scripts.min.js'))
  // .pipe(uglify()) // Mifify js (opt.)
  .pipe(gulp.dest('app/js'))
  .pipe(browsersync.reload({ stream: true }))
});

gulp.task('images', function() {
  return gulp.src('app/img/**/*')
  .pipe(gulp.dest('dist/img')); 
});

gulp.task('rsync', function() {
  return gulp.src('app/**')
  .pipe(rsync({
    root: 'app/',
    hostname: 'username@yousite.com',
    destination: 'yousite/public_html/',
    // include: ['*.htaccess'], // Includes files to deploy
    exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
    recursive: true,
    archive: true,
    silent: false,
    compress: true
  }))
});

gulp.task('watch', ['sass', 'js', 'browser-sync'], function() {
  gulp.watch('app/sass/**/*.sass', ['sass']);
  gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['js']);
  gulp.watch('app/*.html', browsersync.reload)
});

// gulp.task('build', ['removedist', 'images', 'sass', 'js', 'libs-min'], function() {
gulp.task('build', ['removedist', 'images', 'sass', 'js'], function() {

  var buildFiles = gulp.src([
    'app/*.html',
    ]).pipe(gulp.dest('dist'));

  var buildCss = gulp.src([
    'app/css/main.min.css'
    ])
    .pipe(gulp.dest('dist/css'));
  
  var buildJSON = gulp.src([
    'app/news.js'
  ]).pipe(gulp.dest('dist/'));
  // var buildCss = gulp.src([
  //   'app/css/libs.css',
  //   'app/css/main.css',
  //   'app/css/media.css',
  //   ])
  //   .pipe(concat('main.min.css'))
  //   .pipe(gulp.dest('dist/css'));

  var buildJs = gulp.src([
    'app/js/scripts.min.js',
    ]).pipe(gulp.dest('dist/js'));

  var buildFonts = gulp.src([
    'app/fonts/**/*',
    ]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('removedist', function() { return del.sync('dist'); });

gulp.task('default', ['watch']);
